FROM golang:stretch

ENV GOPATH /go
ENV PATH $GOPATH/bin:/root/.yarn/bin:$PATH

RUN curl --silent --location https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update && \
    apt-get install -y git nodejs jq unzip && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    go get -u github.com/jstemmer/go-junit-report

RUN curl -o- -L https://yarnpkg.com/install.sh | bash

RUN npm install -g serverless
